﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LatteGames.EditorUtil;
using System;

namespace LatteGames.Analytics
{
    public abstract class PresetSetting : ScriptableObject {
        public static class CommonAnalyticsPackage{
            public static string Facebook => PackageRootFolderDetection.GetPath() + "Analytics/Analytics/CommonAnalyticsPackages/facebook-unity-sdk-9.1.0.unitypackage";
            public static string GA => PackageRootFolderDetection.GetPath() + "Analytics/Analytics/CommonAnalyticsPackages/AnalyticsManager.unitypackage";
            public static string AppsFlyer => PackageRootFolderDetection.GetPath() + "Analytics/Analytics/CommonAnalyticsPackages/appsflyer-unity-plugin-6.2.41.unitypackage";
            public static string Flurry => PackageRootFolderDetection.GetPath() + "Analytics/Analytics/CommonAnalyticsPackages/flurry-sdk-3.3.0.unitypackage";
            public static string AppMetrica => PackageRootFolderDetection.GetPath() + "Analytics/Analytics/CommonAnalyticsPackages/AppMetrica.unitypackage";
            public static string Gismart => PackageRootFolderDetection.GetPath() + "Analytics/Analytics/CommonAnalyticsPackages/gismart-amplitude-2.24.0.unitypackage";
        }
        public virtual void DrawCustomInspector(){}
        public static class Ultility{
            public static void AddScriptingDefineSymbolForGroup(UnityEditor.BuildTargetGroup group, string newSymbol)
            {
                var currentDefine = new List<string>(PlayerSettings.GetScriptingDefineSymbolsForGroup(group).Split(';'));
                currentDefine.RemoveAll(symbol => string.IsNullOrEmpty(symbol) || string.IsNullOrWhiteSpace(symbol));
                var currentDefineSimplified = new HashSet<string>();
                currentDefine.ForEach(symbol => currentDefineSimplified.Add(symbol));
                currentDefineSimplified.Add(newSymbol);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(group, string.Join(";",new List<string>(currentDefineSimplified).ToArray()));
            }

            public static bool HasDefineSymbol(UnityEditor.BuildTargetGroup group, string define)
            {
                var currentDefine = new List<string>(PlayerSettings.GetScriptingDefineSymbolsForGroup(group).Split(';'));
                currentDefine.RemoveAll(symbol => string.IsNullOrEmpty(symbol) || string.IsNullOrWhiteSpace(symbol));
                return currentDefine.Contains(define);
            }
        }
    }
}