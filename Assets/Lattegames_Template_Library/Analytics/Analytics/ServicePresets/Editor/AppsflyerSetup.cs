﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace LatteGames.Analytics
{
    public static class AppsflyerSetup
    {
        public static void GUI(string appsflyerDevKey, string appsflyerAppId)
        {
#if LatteGames_AppsFlyer
            if(GameObject.FindObjectOfType<AnalyticsManager>() != null)
            {
                var appsflyerService = GameObject.FindObjectOfType<AnalyticsManager>().GetComponentInChildren<AppsflyerAnalyticsService>();
                if(string.IsNullOrEmpty(appsflyerService.DevKey) || string.IsNullOrEmpty(appsflyerService.AppId))
                {
                    EditorGUILayout.HelpBox("Missing Dev key or App id for appsflyer analytics service.",MessageType.Warning);
                }
                if(GUILayout.Button("Set up Appsflyer"))
                {
                    appsflyerService.DevKey = appsflyerDevKey;
                    appsflyerService.AppId = appsflyerAppId;
                    EditorUtility.SetDirty(appsflyerService);
                }
            }
#endif
        }
    }
}