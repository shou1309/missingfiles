﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace LatteGames.Analytics
{
    public static class PackageImportSetup
    {
        public class Package
        {
            public Type ServiceClass;
            public string sdkPath;

            public Package(Type serviceClass, string sdkPath)
            {
                ServiceClass = serviceClass;
                this.sdkPath = sdkPath;
            }
        }
        public static void GUI(params Package[] packages)
        {
            bool packagesImported = true;
            foreach (var package in packages)
            {
                if(!package.ServiceClass.IsSubclassOf(typeof(AnalyticsService)))
                    continue;
                if(!CheckIfDependencyAvailable(package.ServiceClass))
                {
                    packagesImported = false;
                    break;
                }
            }
            if(!packagesImported)
                EditorGUILayout.HelpBox("Missing required SDKs", MessageType.Warning);
            if(GUILayout.Button("Import SDKs"))
            {
                foreach (var package in packages)
                {
                    if(!package.ServiceClass.IsSubclassOf(typeof(AnalyticsService)))
                        continue;
                    if(!CheckIfDependencyAvailable(package.ServiceClass))
                        AssetDatabase.ImportPackage(package.sdkPath, false);
                }
            }
        }
        private static bool CheckIfDependencyAvailable(Type type)
        {
            var target = EditorUserBuildSettings.activeBuildTarget;
            var group = BuildPipeline.GetBuildTargetGroup(target);
            var attributes = new List<object>(Attribute.GetCustomAttributes(type, typeof (OptionalDependencyAttribute))).ConvertAll(attr => attr as OptionalDependencyAttribute);
            var depFullfill = true;
            foreach (var attr in attributes)
            {
                if(!PresetSetting.Ultility.HasDefineSymbol(group, attr.define))
                {
                    depFullfill = false;
                    break;
                }
            }
            return depFullfill;
        }
    }
}