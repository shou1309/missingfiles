﻿using UnityEngine;
using UnityEngine.UI;

public class Scorer : MonoBehaviour
{
    private float score;
    GameObject moneyBag;
    [SerializeField] private float startingScore = 0f;
    [SerializeField] private float moneyLosingSpeed = 650f;

    public ScoreDisplayController scoreDisplayController;
    public DirivedLevelController levelResult;

    private void Start()
    {
        score = startingScore;
        moneyBag = transform.Find("MoneyBag").gameObject;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "money")
        {
            score += 100;
            moneyBag.transform.localScale = new Vector3(moneyBag.transform.localScale.x * 1.05f, moneyBag.transform.localScale.y * 1.05f, moneyBag.transform.localScale.z * 1.05f);
        }
    }

    void Update()
    {
        scoreDisplayController.UpdateScore(score);

        if (levelResult.LevelFinished())
        {
            if ((!levelResult.IsVictory() && score > 0 && gameObject.tag == "Player") || (levelResult.IsVictory() && score > 0 && gameObject.tag == "bot"))
            {
                score -= Time.deltaTime * moneyLosingSpeed;
                if (score < 0)
                    score = 0;
                scoreDisplayController.UpdateScore(score);

                transform.Find("MoneyBag").gameObject.SetActive(true);
                transform.Find("Quai_1").gameObject.SetActive(true);

                moneyBag.transform.localScale = new Vector3(moneyBag.transform.localScale.x * 0.99f, moneyBag.transform.localScale.y * 0.99f, moneyBag.transform.localScale.z * 0.99f);
            }
        }
    }

    public float GetScore()
    {
        return score;
    }

    public void Purchased(float price)
    {
        score -= price;
    }
}
