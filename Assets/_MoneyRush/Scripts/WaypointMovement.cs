﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointMovement : MonoBehaviour
{
    // put the points from unity interface
    public Transform[] wayPointList;
    public PurchaseController[] vehicleList;

    public int currentWayPoint = 0;
    Transform targetWayPoint;
    Scorer scorer;

    public float speed = 11.5f;

    // Use this for initialization
    void Start()
    {
        scorer = GetComponent<Scorer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentWayPoint == this.wayPointList.Length)
        {
            int count = 0;
            foreach (PurchaseController vehicle in vehicleList)
            {
                count++;
                if (count == vehicleList.Length)
                {
                    transform.forward = Vector3.RotateTowards(transform.forward, new Vector3(vehicle.gameObject.transform.position.x, transform.position.y, vehicle.gameObject.transform.position.z) - transform.position, speed * Time.deltaTime, 0.0f);
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(vehicle.gameObject.transform.position.x, transform.position.y, vehicle.gameObject.transform.position.z), speed * Time.deltaTime);
                }
                else if (scorer.GetScore() >= vehicle.GetPrice() && !vehicle.IsPicked())
                {
                    transform.forward = Vector3.RotateTowards(transform.forward, new Vector3(vehicle.gameObject.transform.position.x, transform.position.y, vehicle.gameObject.transform.position.z) - transform.position, speed * Time.deltaTime, 0.0f);
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(vehicle.gameObject.transform.position.x, transform.position.y, vehicle.gameObject.transform.position.z), speed * Time.deltaTime);
                    break;
                }
            }
        }
        // check if we have somewere to walk
        else if (currentWayPoint < this.wayPointList.Length)
        {
            if (targetWayPoint == null)
                targetWayPoint = wayPointList[currentWayPoint];
            walk();
        }
    }

    void walk()
    {
        // rotate towards the target
        transform.forward = Vector3.RotateTowards(transform.forward, targetWayPoint.position - transform.position, speed * Time.deltaTime, 0.0f);

        // move towards the target
        transform.position = Vector3.MoveTowards(transform.position, targetWayPoint.position, speed * Time.deltaTime);

        if (transform.position == targetWayPoint.position)
        {
            currentWayPoint++;
            if(currentWayPoint < this.wayPointList.Length - 1)
                targetWayPoint = wayPointList[currentWayPoint];
        }
    }
}
