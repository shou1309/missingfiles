﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotController : MonoBehaviour
{
    //PlayerMovement playerMovement;
    NavMeshAgent navMeshAgent;
    Scorer scorer;
    public Transform decidePos;
    public PurchaseController[] vehicleList;

    void Start()
    {
        //playerMovement = GetComponent<PlayerMovement>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        scorer = GetComponent<Scorer>();
        //playerMovement.enabled = false;
        navMeshAgent.enabled = true;

        navMeshAgent.SetDestination(decidePos.position);
    }

    public void BotReturnCheckpoint(Vector3 cp)
    {
        navMeshAgent.Warp(cp);
        navMeshAgent.SetDestination(decidePos.position);
    }

    void Update()
    {
        if (transform.position.x == decidePos.position.x)
        {
            int count = 0;
            foreach (PurchaseController vehicle in vehicleList)
            {
                count++;
                if(count == vehicleList.Length)
                {
                    navMeshAgent.SetDestination(vehicle.gameObject.transform.position);
                }
                else if (scorer.GetScore() >= vehicle.GetPrice() && !vehicle.IsPicked())
                {
                    navMeshAgent.SetDestination(vehicle.gameObject.transform.position);
                    break;
                }
            }
        }
    }
}
