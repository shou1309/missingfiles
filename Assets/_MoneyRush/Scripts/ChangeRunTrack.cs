﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class ChangeRunTrack : MonoBehaviour
{
    public Transform decidePos;
    public Canvas speedGauge;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerMovement>().enabled = true;
            other.gameObject.GetComponent<TapMovement>().enabled = false;
            other.gameObject.GetComponent<LockChildRotation>().enabled = false;
            other.gameObject.GetComponent<TapMovement>().enabled = false;
            other.gameObject.GetComponent<TapMovement>().DisableVehicle();
            speedGauge.enabled = false;
            if (other.gameObject.GetComponent<Animator>().GetBool("IsSkating"))
            {
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                other.gameObject.GetComponent<Rigidbody>().position += new Vector3(0, -0.3f, 0);
            }
            if (other.gameObject.GetComponent<Animator>().GetBool("IsRiding"))
            {
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                other.gameObject.GetComponent<Rigidbody>().position += new Vector3(0, -1.1f, 0);
            }
            other.gameObject.GetComponent<Animator>().SetBool("IsRunning", true);
            other.gameObject.GetComponent<Animator>().SetBool("IsJetting", false);
            other.gameObject.GetComponent<Animator>().SetBool("IsDriving", false);
            other.gameObject.GetComponent<Animator>().SetBool("IsRiding", false);
            other.gameObject.GetComponent<Animator>().SetBool("IsSkating", false);
            StartCoroutine(WaitUpdatePosition(other.gameObject, 0.001f));
        }
        else if (other.gameObject.tag == "bot")
        {
            other.gameObject.GetComponent<BotController>().enabled = true;
            other.gameObject.GetComponent<TapMovement>().enabled = false;
            other.gameObject.GetComponent<LockChildRotation>().enabled = false;
            other.gameObject.GetComponent<TapMovement>().enabled = false;
            other.gameObject.GetComponent<TapMovement>().DisableVehicle();
            if (other.gameObject.GetComponent<Animator>().GetBool("IsSkating"))
            {
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                other.gameObject.GetComponent<Rigidbody>().position += new Vector3(0, -0.3f, 0);
            }
            if (other.gameObject.GetComponent<Animator>().GetBool("IsRiding"))
            {
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                other.gameObject.GetComponent<Rigidbody>().position += new Vector3(0, -1.1f, 0);
            }
            other.gameObject.GetComponent<Animator>().SetBool("IsRunning", true);
            other.gameObject.GetComponent<Animator>().SetBool("IsJetting", false);
            other.gameObject.GetComponent<Animator>().SetBool("IsDriving", false);
            other.gameObject.GetComponent<Animator>().SetBool("IsRiding", false);
            other.gameObject.GetComponent<Animator>().SetBool("IsSkating", false);
            other.gameObject.GetComponent<NavMeshAgent>().enabled = true;
            other.gameObject.GetComponent<NavMeshAgent>().SetDestination(decidePos.position);
            StartCoroutine(WaitUpdatePosition(other.gameObject, 0.01f));
        }
    }

    IEnumerator WaitUpdatePosition(GameObject gameObject, float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }
}
