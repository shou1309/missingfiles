﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyController : MonoBehaviour
{
    public ParticleSystem CollectMoney;
    BoxCollider collid;
    MeshRenderer meshRenderer;
    AudioSource money;

    void Start()
    {
        collid = GetComponent<BoxCollider>();
        meshRenderer = GetComponent<MeshRenderer>();
        money = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "bot")
        {
            if (other.gameObject.tag == "Player")
                money.Play();

            Collect();
        }
    }

    void Collect()
    {
        CollectMoney.Play();
        StartCoroutine(StopParticleSystem(CollectMoney, 0.5f));
        //gameObject.SetActive(false);
        collid.enabled = false;
        meshRenderer.enabled = false;
    }

    IEnumerator StopParticleSystem(ParticleSystem particleSystem, float time)
    {
        yield return new WaitForSeconds(time);
        particleSystem.Stop();
    }
}


