﻿using UnityEngine;
using UnityEngine.UI;

public class SpeedGaugeController : MonoBehaviour
{
    public TapMovement tapMovement;

    Slider speedGauge;

    void Start()
    {
        speedGauge = GetComponent<Slider>();
        speedGauge.maxValue = tapMovement.GetMaxVelocity();
        speedGauge.minValue = tapMovement.GetMinVelocity();
    }

    void Update()
    {
        speedGauge.value = tapMovement.GetCurrentVelocity();
    }
}
