﻿using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    CinemachineVirtualCamera cam;
    public Transform player;

    void Start() {
        cam =  GetComponent<CinemachineVirtualCamera>();
    }

    public void FocusOnWinner()
    {
        cam.Follow = player;
        cam.LookAt = player;
        cam.GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance = 10f;
        cam.GetCinemachineComponent<CinemachineFramingTransposer>().m_TrackedObjectOffset = new Vector3(0.01199833f, 2, 2);
    }
}
