﻿using System;
using UnityEngine;

public class LockChildRotation : MonoBehaviour
{
    Quaternion canvasAngle = Quaternion.Euler(0f, -90f, 0f); 

    void Update()
    {
        transform.rotation = canvasAngle;
    }

    public void SetDirection(float YAxis)
    {
        canvasAngle = Quaternion.Euler(0f, YAxis, 0f);
    }
}
