﻿using System;
using UnityEngine;

public class TapMovement : MonoBehaviour
{
    Rigidbody rigidBody;
    Animator animator;
    Vector3 movement;

    public GameObject speedGauge;
    public DirivedLevelController dirivedLevelController;

    private string chosenVehicle;
    private string currentAnimation;
    //[SerializeField] private float runSpeed = 0.2f;
    private float minVelocity;
    private float maxVelocity;
    [SerializeField] private int BotTapPerSec = 3;
    private float currentVelocity;
    private float targetVelocity;
    private float acceleration = 0f;
    [SerializeField] private float CarMin = 0.4f;
    [SerializeField] private float CarMax = 0.8f;
    [SerializeField] private float CarAcceleration = 0.009f;
    [SerializeField] private float BikeMin = 0.3f;
    [SerializeField] private float BikeMax = 0.6f;
    [SerializeField] private float BikeAcceleration = 0.007f;
    [SerializeField] private float JetpackMin = 0.5f;
    [SerializeField] private float JetpackMax = 1f;
    [SerializeField] private float JetpackAcceleration = 0.01f;
    [SerializeField] private float SkateMin = 0.2f;
    [SerializeField] private float SkateMax = 0.4f;
    [SerializeField] private float SkateAcceleration = 0.004f;
    [SerializeField] private float FootMin = 0.2f;
    [SerializeField] private float FootMax = 0.3f;
    [SerializeField] private float FootAcceleration = 0.003f;
    private float timer = 0;
    private int tapCounter = 0;
    private float tapPerDuration = 0;

    public void UseVehicle(string vehicle)
    {
        if (gameObject.tag == "Player")
        {
            speedGauge.SetActive(true);
            transform.Find("New Sprite").gameObject.SetActive(false);
        }

        rigidBody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();

        rigidBody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        switch (vehicle)
        {
            case "Car":
                EnableVehicle(vehicle, "IsDriving", CarAcceleration, CarMin, CarMax);
                break;
            case "Skate":
                EnableVehicle(vehicle, "IsSkating", SkateAcceleration, SkateMin, SkateMax);
                break;
            case "Motorbike":
                EnableVehicle(vehicle, "IsRiding", BikeAcceleration, BikeMin, BikeMax);
                break;
            case "Jetpack":
                EnableVehicle(vehicle, "IsJetting", JetpackAcceleration, JetpackMin, JetpackMax);
                break;
            case "Foot":
                currentAnimation = "IsRunning";
                movement.Set(-currentVelocity, 0, 0);
                GetComponent<LockChildRotation>().enabled = true;
                minVelocity = FootMin;
                maxVelocity = FootMax;
                currentVelocity = minVelocity;
                acceleration = FootAcceleration;
                break;
            default:
                Debug.Log("TYPING ERROR!!!");
                break;
        }
    }

    void EnableVehicle(string vehicle, string animation, float accel, float MinMod, float MaxMod)
    {
        minVelocity = MinMod;
        maxVelocity = MaxMod;

        chosenVehicle = vehicle;
        currentAnimation = animation;

        currentVelocity = minVelocity;
        targetVelocity = minVelocity;

        movement.Set(-currentVelocity, 0, 0);

        GetComponent<LockChildRotation>().enabled = true;

        transform.Find(vehicle).gameObject.SetActive(true);
        transform.Find("MoneyBag").gameObject.SetActive(false);
        transform.Find("Quai_1").gameObject.SetActive(false);

        animator.SetBool(animation, true);
        animator.SetBool("IsRunning", false);

        acceleration = accel;
    }

    public void DisableVehicle()
    {
        if (!String.IsNullOrEmpty(chosenVehicle))
        {
            transform.Find(chosenVehicle).gameObject.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        if (dirivedLevelController.IsBotWon() && gameObject.tag == "Player")
        {
            animator.SetBool("IsLost", true);
            animator.SetBool(currentAnimation, false);
            DisableVehicle();
            GetComponent<LockChildRotation>().enabled = true;
            gameObject.GetComponent<LockChildRotation>().SetDirection(90);
            GetComponent<TapMovement>().enabled = false;
        }

        timer += Time.fixedDeltaTime;

        if (timer < 1f)
        {
            if (Input.GetMouseButtonDown(0) && gameObject.tag == "Player")
                tapCounter++;
        }
        else
        {
            if (gameObject.tag == "bot")
            {
                tapCounter = BotTapPerSec;
            }
            Debug.Log("current: " + currentVelocity + "min: " + minVelocity + "max: " + maxVelocity);

            tapPerDuration = tapCounter / timer;
            tapCounter = 0;
            timer = 0;
        }

        targetVelocity = tapPerDuration * minVelocity;

        if (currentVelocity < maxVelocity && targetVelocity > currentVelocity)
        {
            currentVelocity += (targetVelocity - currentVelocity) * acceleration;
        }

        if (currentVelocity > minVelocity)
        {
            currentVelocity -= Time.fixedDeltaTime * 0.1f;
        }

        movement.Set(-currentVelocity, 0, 0);
        rigidBody.MovePosition(rigidBody.position + movement);
    }

    // void OnAnimatorMove()
    // {
    // }

    public float GetMinVelocity()
    {
        return minVelocity;
    }

    public float GetMaxVelocity()
    {
        return maxVelocity;
    }

    public float GetCurrentVelocity()
    {
        return currentVelocity;
    }
}
