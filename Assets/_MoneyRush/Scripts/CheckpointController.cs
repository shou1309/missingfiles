﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CheckpointController : MonoBehaviour
{
    public Transform[] checkpoints;
    Rigidbody rigidBody;
    BotController botController;
    Animator animator;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        botController = GetComponent<BotController>();
        animator = GetComponent<Animator>();
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "obstacle")
        {
            animator.SetBool("IsHitted", true);
            animator.SetBool("IsRunning", false);
            transform.Find("MoneyBag").gameObject.SetActive(false);
            transform.Find("Quai_1").gameObject.SetActive(false);

            // if (gameObject.tag == "bot")
            //     gameObject.GetComponent<NavMeshAgent>().isStopped = true;

            if (gameObject.tag == "bot")
                GetComponent<WaypointMovement>().enabled = false;
            StartCoroutine(WaitCharacterFall(1f));
        }
    }

    public void ReturnCheckpoint()
    {
        foreach (Transform checkpoint in checkpoints)
        {
            if (checkpoint.position.x >= rigidBody.position.x)
            {
                animator.SetBool("IsHitted", false);
                animator.SetBool("IsRunning", true);
                if (gameObject.tag == "Player")
                    rigidBody.position = checkpoint.position;
                else if (gameObject.tag == "bot")
                {
                    rigidBody.position = checkpoint.position;
                    GetComponent<WaypointMovement>().enabled = true;
                    //botController.BotReturnCheckpoint(checkpoint.position);
                }
                break;
            }
        }
    }

    IEnumerator WaitCharacterFall(float time)
    {
        yield return new WaitForSeconds(time);
        transform.Find("MoneyBag").gameObject.SetActive(true);
        transform.Find("Quai_1").gameObject.SetActive(true);
        ReturnCheckpoint();
    }
}
