﻿using UnityEngine;

public class SpinnerY : MonoBehaviour
{
    [SerializeField] float spinMod = 1f;
    void Update()
    {
        transform.Rotate(0, spinMod, 0);
    }
}
