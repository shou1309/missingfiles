﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public DirivedLevelController dirivedLevelController;

    void Update()
    {
        if (dirivedLevelController.IsVictory())
        {
            GetComponent<Text>().text = "YOU WIN";
        }
    }
}
