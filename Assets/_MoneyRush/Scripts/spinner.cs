using UnityEngine;

public class spinner : MonoBehaviour
{
    [SerializeField] float spinMod = 1f;
    void Update()
    {
        transform.Rotate(spinMod, 0, 0);
    }
}
