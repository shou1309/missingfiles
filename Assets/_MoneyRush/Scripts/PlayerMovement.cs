﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    Vector3 movement;
    Vector3 mouseDown;

    public Canvas speedGauge;
    public CameraController cameraController;
    public DirivedLevelController dirivedLevelController;

    Quaternion rotation = Quaternion.identity;

    private float moveXAxis = -0.5f;
    [SerializeField] private float turnSpeed = 10f;
    [SerializeField] private float runSpeed = 1f;
    [SerializeField] private float sensitivity = 100f;
    [SerializeField] private float zClampMod = 5.5f;
    private float horizontal;

    private bool dragging;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            if (gameObject.GetComponent<Animator>().GetBool("IsSkating"))
            {
                gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                gameObject.GetComponent<Rigidbody>().position += new Vector3(0, -0.3f, 0);
            }
            if (gameObject.GetComponent<Animator>().GetBool("IsRiding"))
            {
                gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                gameObject.GetComponent<Rigidbody>().position += new Vector3(0, -1.1f, 0);
            }
            StartCoroutine(WaitUpdatePosition(0.001f));

            if (gameObject.tag == "Player")
            {
                speedGauge.enabled = false;
                gameObject.GetComponent<LockChildRotation>().enabled = true;
                gameObject.GetComponent<LockChildRotation>().SetDirection(90);
                if (!dirivedLevelController.IsBotWon())
                {
                    cameraController.FocusOnWinner();
                    dirivedLevelController.EndLevel(true);
                    dirivedLevelController.PlayerFinishIntime();
                }
                gameObject.GetComponent<PlayerMovement>().enabled = false;
            }
            else if (gameObject.tag == "bot")
            {
                gameObject.GetComponent<NavMeshAgent>().enabled = false;
                StartCoroutine(BotFinishingFirst(gameObject, cameraController, 0));
            }

            GetComponent<TapMovement>().DisableVehicle();
            GetComponent<TapMovement>().enabled = false;

            GetComponent<Animator>().SetBool("IsFinished", true);
        }
    }

    void Update()
    {
        if(dirivedLevelController.IsBotWon())
        {
            GetComponent<LockChildRotation>().enabled = true;
            gameObject.GetComponent<LockChildRotation>().SetDirection(90);
            GetComponent<Animator>().SetBool("IsLost", true);
            GetComponent<Animator>().SetBool("IsRunning", false);
        }

        ZAxisClamp();

        if (Input.GetMouseButtonDown(0))
        {
            dragging = true;
            mouseDown = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
            horizontal = 0;
        }
        if (dragging)
        {
            horizontal = (Input.mousePosition - mouseDown).x / sensitivity;
        }

        movement.Set(moveXAxis, 0, horizontal);
        movement.Normalize();

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, movement, turnSpeed * Time.deltaTime, 0f);
        rotation = Quaternion.LookRotation(desiredForward);
    }

    private void ZAxisClamp()
    {
        Vector3 currentPosition = GetComponent<Rigidbody>().position;
        float clampedZ = Mathf.Clamp(currentPosition.z, -zClampMod, zClampMod);

        if (!Mathf.Approximately(clampedZ, currentPosition.z))
        {
            currentPosition.z = clampedZ;
            GetComponent<Rigidbody>().position = currentPosition;
        }
    }

    void OnAnimatorMove()
    {
        GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + movement * GetComponent<Animator>().deltaPosition.magnitude * runSpeed);
        GetComponent<Rigidbody>().MoveRotation(rotation);
    }

    IEnumerator BotFinishingFirst(GameObject bot, CameraController cameraController, float time)
    {
        yield return new WaitForSeconds(time);
        if (!dirivedLevelController.PlayerFinished())
        {
            cameraController.FocusOnWinner();
            dirivedLevelController.BotWins();
            dirivedLevelController.EndLevel(false);
        }
    }

     IEnumerator WaitUpdatePosition(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }
}
