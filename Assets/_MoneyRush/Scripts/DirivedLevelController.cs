﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirivedLevelController : LatteGames.LevelController
{
    private bool win = false;
    private bool levelEnds = false;
    private bool PlayerFinishInTime = false;
    private bool Botwins = false;

    public override void StartLevel()
    {
        base.StartLevel();
    }
    public void EndLevel(bool win)
    {
        levelEnds = true;
        this.win = win;
        base.EndLevel();
    }
    public override void PauseLevel()
    {
        base.PauseLevel();
    }
    public override void ResumeLevel()
    {
        base.ResumeLevel();
    }
    public override bool IsVictory()
    {
        return win;
    }

    public bool LevelFinished()
    {
        return levelEnds;
    }

    public void PlayerFinishIntime()
    {
        PlayerFinishInTime = true;
    }

    public bool PlayerFinished()
    {
        return PlayerFinishInTime;
    }

    public void BotWins()
    {
        Botwins = true;
    }

    public bool IsBotWon()
    {
        return Botwins;
    }
}
