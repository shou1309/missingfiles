﻿using UnityEngine;

public class SawSpinner : MonoBehaviour
{
    [SerializeField] float spinMod = 3f;
    void Update()
    {
        transform.Rotate(0, 0, spinMod);
    }
}
