﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PurchaseController : MonoBehaviour
{
    [SerializeField] private float price = 0f;
    [SerializeField] private string vehicle = "";

    AudioSource purchase;
    Rigidbody rigidBody;
    Scorer scorer;
    PlayerMovement playerMovement;
    TapMovement tapMovement;
    BotController botController;
    Animator animator;
    NavMeshAgent navMeshAgent;
    CheckpointController checkpointController;
    WaypointMovement waypointMovement;
    private Vector3 Pushback;

    private bool isPicked;

    void Start()
    {
        purchase = GetComponent<AudioSource>();
        isPicked = false;
        Pushback = new Vector3(-50f, 0, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        checkpointController = other.gameObject.GetComponent<CheckpointController>();

        if (isPicked)
        {
            checkpointController.ReturnCheckpoint();
        }

        else if (other.gameObject.tag == "Player" || other.gameObject.tag == "bot")
        {
            rigidBody = other.gameObject.GetComponent<Rigidbody>();
            scorer = other.gameObject.GetComponent<Scorer>();
            playerMovement = other.gameObject.GetComponent<PlayerMovement>();
            tapMovement = other.gameObject.GetComponent<TapMovement>();
            botController = other.gameObject.GetComponent<BotController>();
            animator = other.gameObject.GetComponent<Animator>();
            navMeshAgent = other.gameObject.GetComponent<NavMeshAgent>();

            isPicked = true;

            playerMovement.enabled = false;
            botController.enabled = false;
            navMeshAgent.enabled = false;
            if (other.gameObject.tag == "bot")
                other.gameObject.GetComponent<WaypointMovement>().enabled = false;

            if (scorer.GetScore() >= price)
            {
                if (other.gameObject.tag == "Player")
                    purchase.Play();

                scorer.Purchased(price);
                transform.GetChild(0).gameObject.SetActive(false);
                transform.GetChild(1).gameObject.SetActive(false);
                transform.GetChild(2).gameObject.SetActive(true);

                if (string.Equals(vehicle, "Car"))
                {
                    float offsetCar = 0.6f - gameObject.transform.parent.gameObject.transform.position.z + rigidBody.position.z;
                    rigidBody.position += new Vector3(0, 0, -offsetCar);
                }
                else
                {
                    float offset = -gameObject.transform.parent.gameObject.transform.position.z + rigidBody.position.z;
                    rigidBody.position += new Vector3(0, 0, -offset);

                    if (string.Equals(vehicle, "Skate"))
                    {
                        rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                        rigidBody.position += new Vector3(0, 0.3f, 0);
                    }
                    if (string.Equals(vehicle, "Motorbike"))
                    {
                        rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                        rigidBody.position += new Vector3(0, 1.1f, 0);
                    }
                }
                StartCoroutine(WaitUpdatePosition(vehicle, 0.01f));
            }
            else
            {
                float offset = -gameObject.transform.parent.gameObject.transform.position.z + rigidBody.position.z;
                rigidBody.position += new Vector3(0, 0, -offset);
                transform.GetChild(0).gameObject.SetActive(false);
                transform.GetChild(1).gameObject.SetActive(false);
                StartCoroutine(WaitUpdatePosition("Foot", 0.01f));

                // playerMovement.enabled = false;
                // animator.enabled = false;
                // rigidBody.AddForce(Pushback); DOESN'T WORK !!!!
                // StartCoroutine(ResumeForwardMovement(playerMovement, 10f));
            }
        }
    }

    IEnumerator ResumeForwardMovement(PlayerMovement playerMovement, float time)
    {
        yield return new WaitForSeconds(time);
        playerMovement.enabled = true;
        animator.enabled = true;
    }

    IEnumerator WaitUpdatePosition(string vehicle, float time)
    {
        yield return new WaitForSeconds(time);
        tapMovement.enabled = true;
        tapMovement.UseVehicle(vehicle);
    }

    public float GetPrice()
    {
        return price;
    }

    public string GetVehicle()
    {
        return vehicle;
    }

    public bool IsPicked()
    {
        return isPicked;
    }
}
