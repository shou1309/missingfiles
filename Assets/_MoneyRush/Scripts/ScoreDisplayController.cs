﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreDisplayController : MonoBehaviour
{
    TMP_Text txt;

    void Start()
    {
        txt = GetComponent<TMP_Text>();
    }

    public void UpdateScore(float money)
    {
        txt.SetText("$" + money.ToString("0"));
    }
}
