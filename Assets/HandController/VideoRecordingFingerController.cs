﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (LazyTransform))]
public class VideoRecordingFingerController : MonoBehaviour {
    public LazyTransform lazyTransform;
    public float mouseDownScale = 0.9f;
    public GameObject handObject;
    public GameObject Circle;
    public Animator animator;

    void  Start() 
    {
        Circle.SetActive(false);
        handObject.SetActive(false);
    }

    private void Update () {

        if(Input.GetKeyDown(KeyCode.Space))
        {
            handObject.SetActive(false);
            Circle.SetActive(false);
        }

        if(Input.GetMouseButtonDown(0))
        {
            if (hideHandCR != null)
            {
                StopCoroutine(hideHandCR);
                hideHandCR = null;
            }

            ShowHand();
            animator.SetTrigger("TapTrigger");
        }

        if(Input.GetMouseButtonUp(0))
        {
            HideHand();
        }

        lazyTransform.Position = Input.mousePosition;
        if (Input.GetMouseButtonDown (0)) {
            lazyTransform.LocalScale = Vector3.one * mouseDownScale;
        }
        if (Input.GetMouseButtonUp (0)) {
            lazyTransform.LocalScale = Vector3.one;
        }
    }

    public void ShowHand()
    {
        Circle.SetActive(true);
        handObject.SetActive(true);
    }

    Coroutine showHandCR;

    IEnumerator CR_ShowHand()
    {
        Circle.SetActive(true);
        handObject.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        handObject.SetActive(false);
        Circle.SetActive(false);
        showHandCR = null;
    }

    public void HideHand()
    {
        if(hideHandCR != null)
        {
            StopCoroutine(hideHandCR);
            hideHandCR = null;
        }
        hideHandCR = StartCoroutine(CR_HideHand());
    }

    Coroutine hideHandCR;
    IEnumerator CR_HideHand()
    {
        yield return new WaitForSeconds(0.3f);
        handObject.SetActive(false);
        Circle.SetActive(false);
        hideHandCR = null;
    }
}