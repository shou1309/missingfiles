﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideGauge : MonoBehaviour
{
    public static HideGauge Instance { get; protected set; }
    [SerializeField] protected GameObject gaugeSpeed;
    void Awake() {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    } 

    public void Hide()
    {
        gaugeSpeed.SetActive(false);
    }
}
